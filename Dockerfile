FROM php:7.4.1-fpm-alpine

COPY . /

CMD php -S 0.0.0.0:80 app/public/index.php
