<?php

namespace App\Controller;

use App\Document\ToDo;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/to-do")
 */
class ToDoController extends AbstractController
{
    private $dm;

    public function __construct(DocumentManager $manager)
    {
        $this->dm = $manager;
    }

    /**
     * @Route("/", name="todo_index")
     * @throws ExceptionInterface
     */
    public function index(): Response
    {
        $todos = $this->dm->getRepository(ToDo::class)->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $response = $serializer->normalize($todos, 'json', ['attributes' => [
            'id', 'title', 'description', 'createdAt', 'plannedOn','isDone',
            'user' => ['id', 'firstName', 'lastName']]]);
        return new JsonResponse($response);
    }

    /**
     * @Route("/create-todo",name="create_to_do",methods={"POST"})
     * @param Request $request
     * @return JsonResponse|Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function createToDo(Request $request): Response
    {
        $params = json_decode($request->getContent(), true);
        $user = $this->dm->getRepository(User::class)->find($params['user']['id']);
        if ($user instanceof User) {
            $todo = new ToDo();
            $todo->setUser($user);
            $date = new \DateTime('now');
//            $plannedOn = new \DateTime($params['plannedOn']);
            $todo->setCreatedAt($date);
            $todo->setDescription($params['description']);
            //$todo->setPlannedOn($plannedOn->format(\DateTime::ISO8601));
            $todo->setTitle($params['title']);
            $todo->setIsDone($params['isDone']);
            $this->dm->persist($todo);
            $this->dm->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $response = $serializer->normalize($todo, 'json', ['attributes' => [
                'id', 'title', 'description', 'createdAt', 'isDone','plannedOn',
                'user' => ['firstName', 'lastName']]]);
            return new JsonResponse($response);
        } else {
            return new Response("couldn't create the todo item , please check out your parameters");
        }
    }

    /**
     * @Route("/update/{id}",name="update_todo",methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return JsonResponse|Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function updateToDoList(Request $request, $id)
    {
        $todo = $this->dm->getRepository(ToDo::class)->find($id);
        $params = json_decode($request->getContent(), true);
        if ($todo instanceof ToDo) {
            $plannedOn = new \DateTime($params['plannedOn']);
            $todo->setTitle($params['title']);
            $todo->setDescription($params['description']);
//            $todo->setPlannedOn($plannedOn->format(\DateTime::ISO8601));
            $todo->setIsDone($params['isDone']);
            $this->dm->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $response = $serializer->normalize($todo, 'json', ['attributes' => [
                'id', 'title', 'description', 'createdAt', 'plannedOn','isDone',
                'user' => ['firstName', 'lastName']]]);
            return new JsonResponse($response);
        } else return new Response("Couldn't update the todo item , please check your parameters");
    }

    /**
     * @Route("/remove/{id}",name="task_done",methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     */
    public function markTodoDone(Request $request, $id): Response
    {
        $todo = $this->dm->getRepository(ToDo::class)->find($id);
        if ($todo instanceof ToDo) {
            $this->dm->remove($todo);
            $this->dm->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $response = $serializer->normalize($todo, 'json', ['attributes' => [
                'id', 'title', 'description', 'createdAt', 'plannedOn','isDone',
                'user' => ['firstName', 'lastName']]]);
            return new JsonResponse($response);
        }
        else return new Response("Couldn't mark the todo item as done , please check your id reference !");
    }

}
