<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UtilsController extends AbstractController
{
    /**
     * @Route(path="/api/login_check",name="login")
     */
    public function login(): JsonResponse
    {
        $user = $this->getUser();
        return new JsonResponse(
            array(
                'email'=>$user->getUsername(),
                'roles'=>$user->getRoles()
            )
        );
    }
}
