<?php

namespace App\Controller;

use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
     * @Route("/user")
     */
class UserController extends AbstractController
{
    private $dm;
    private $passwordEncoder;
    public function __construct(DocumentManager $manager,UserPasswordEncoderInterface $encoder)
    {
        $this->dm = $manager;
        $this->passwordEncoder = $encoder;
    }

    /**
     * @Route("/", name="users_fetch",methods={"GET"})
     * @throws ExceptionInterface
     */
    public function index(): Response
    {
        $users = $this->dm->getRepository(User::class)->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $response = $serializer->normalize($users,'json',['attributes'=>['email','name','lastName','id']]);
        return new JsonResponse($response);
    }

    /**
     * @Route("/new-user", name="new_user",methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     */
    public function newUser(Request  $request):Response{
        $user = new User();
        $parameters = json_decode($request->getContent(), true);
        $user->setEmail($parameters['email']);
        $user->setPassword($this->passwordEncoder->encodePassword($user,$parameters['password']));
        $user->setName($parameters['name']);
        $user->setLastName($parameters['lastName']);
        $this->dm->persist($user);
        $this->dm->flush();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $response = $serializer->normalize($user,'json',['attributes'=>['email','name','lastName','id']]);
        return new JsonResponse($response);
    }

    /**
     * @Route("/update/{id}", name="update_user",methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     */
    public function updateUser(Request $request,$id): Response
    {
        $params = json_decode($request->getContent(),true);
        $user = $this->dm->getRepository(User::class)->find($id);
        if ($user instanceof  User){
            $user->setName($params['name']);
            $user->setPassword($this->passwordEncoder->encodePassword($user,$params['password']));
            $user->setEmail($params['email']);
            $user->setLastName($params['lastName']);
            $this->dm->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $response = $serializer->normalize($user,'json',['attributes'=>['id','email','name','lastName']]);
            return new JsonResponse($response);
        }
        else return new Response("Couldn't update the user , check out your passed parameters !");
    }

    /**
     * @Route("/update/{id}", name="remove_user",methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return Response
     * @throws MongoDBException
     * @throws ExceptionInterface
     */
    public function deleteUser(Request $request,$id) : Response{
        $user = $this->dm->getRepository(User::class)->find($id);
        if ($user instanceof User){
            $serializer = new Serializer([new ObjectNormalizer()]);
            $response = $serializer->normalize($user,'json',['attributes'=>['email','name','lastName']]);
            $this->dm->remove($user);
            $this->dm->flush();
            return new JsonResponse($response);
        }
        else return new Response("Couldn't delete the user , please check out the passed id ! ");
    }







}
